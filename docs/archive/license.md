---
title: "License"
---

# License

This code is distributed under the MIT license, see the [LICENSE](https://gitlab.com/meltano/meltano/blob/master/LICENSE) file.
